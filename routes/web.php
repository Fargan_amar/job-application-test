<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::middleware(['auth'])->group(function(){
    //teacher
    Route::get('/teacher', 'TeacherController@index');
    Route::post('/add-teacher' , 'TeacherController@addTeacher');
    Route::post('/edit-teacher' , 'TeacherController@editTeacher');
    Route::get('/delete-teacher/{id}', 'TeacherController@deleteTeacher');

    //student
    Route::get('/student', 'StudentController@index');
    Route::post('/add-student', 'StudentController@addStudent');
    Route::post('/edit-student', 'StudentController@editStudent');
    Route::get('/delete-student/{id}', 'StudentController@deleteStudent');

    //classroom
    Route::get('/classroom', 'ClassroomController@index');
    Route::post('/add-classroom', 'ClassroomController@addClassroom');
    Route::post('/edit-classroom', 'ClassroomController@editClassroom');
    Route::get('/delete-classroom/{id}', 'ClassroomController@deleteClassroom');

    //export to pdf
    Route::get('/export-classroom' , 'ClassroomController@exportToPDF');

});
