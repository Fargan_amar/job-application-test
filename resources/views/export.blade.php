<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
        }

        td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
        }

        tr:nth-child(even) {
        background-color: #dddddd;
        }
</style>
</head>
<body>
    <h3>List of Classroom with Teachers and Students</h3>
    <table style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Teacher</th>
                <th>Student</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($fetch_all_classroom as $item)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item->name}}</td>
                    @if (empty($item->teachers->name) )
                        <td></td>
                    @else
                    <td>{{$item->teachers->name}}</td>
                    @endif
                    @if (empty($item->students->name))
                        <td></td>
                    @else
                    <td>{{$item->students->name}}</td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
