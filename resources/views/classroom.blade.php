@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">classroom</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add New classroom</button>
                    <a href="{{url('/export-classroom')}}"><button type="button" class="btn btn-info btn-lg" >Export Data as PDF</button></a>

                    <table style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Teacher</th>
                                <th>Student</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($fetch_all_classroom as $item)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$item->name}}</td>
                                    @if (empty($item->teachers->name) )
                                     <td></td>
                                    @else
                                    <td>{{$item->teachers->name}}</td>
                                    @endif
                                    @if (empty($item->students->name))
                                     <td></td>
                                    @else
                                    <td>{{$item->students->name}}</td>
                                    @endif
                                    <td>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#edit{{$item->id}}">Edit classroom</button>
                                        <a href="{{url('/delete-classroom' , $item->id)}}"><button type="button" class="btn btn-danger btn-sm">Delete classroom</button></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Add classroom</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
             <form action="{{url('add-classroom')}}" method="POST">
                 @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="email" required>
                </div>
                <div class="form-group">
                    <label for="name">Choose Teacher</label>
                    <select name="teacher_id" id="">
                        <option value=""></option>
                        @foreach ($fetch_teacher as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Choose Student</label>
                    <select name="student_id" id="">
                        <option value=""></option>
                        @foreach ($fetch_student as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>

    </div>
</div>
@foreach ($fetch_all_classroom as $value)
<div id="edit{{$value->id}}" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit classroom</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{url('edit-classroom')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$value->id}}">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" value="{{$value->name}}" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Choose Teacher</label>
                        <select name="teacher" id="">
                            <option value=""></option>
                            @foreach ($fetch_teacher as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Choose Student</label>
                        <select name="student" id="">
                            <option value=""></option>
                            @if (!empty($item->students->name))
                                <option value="{{$value->students->id}}" selected>{{$value->students->name}}</option>
                            @endif
                            @foreach ($fetch_student as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@endforeach
@endsection
