# How to run this folder

   __Clone this repository to work on.__
   - run "composer install" to your repository from command prompt or terminal
   - run "php artisan key:generate" from command prompt or terminal inside your repository
   - create new file .env
   - copy all text inside .env.example to .env  
   - configure your database inside .env file 
   - run migration with the following command "php artisan migrate" from command prompt or terminal inside your repository
   - run seeder with the following command "php db:seed" from command prompt or terminal inside your repository
   - you can login with the following email : admin@mail.com and password : 12345678
   - once you have been loggin to the system you can do anything, such as : CRUD for teachers, students, and classroom. You can also do export the classroom data into pdf format with clicking the "export data as PDF" on classroom page
