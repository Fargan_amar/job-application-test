<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    protected $fillable = [
        'name'
    ];

    public function students()
    {
        return $this->belongsTo('App\Student', 'student_id');
    }

    public function teachers()
    {
        return $this->belongsTo('App\Teacher', 'teacher_id');
    }

}
