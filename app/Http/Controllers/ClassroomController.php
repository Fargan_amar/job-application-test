<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classroom;
use App\Student;
use App\Teacher;
use PDF;

class ClassroomController extends Controller
{
    public function index()
    {
        $fetch_all_classroom = Classroom::all();

        //array list for student id exists on classroom table
        $student_id = array();
        foreach ($fetch_all_classroom as $key => $value) {
            if($value->studen_id == null){
            }else{
                array_push($student_id, $value->student_id);
            }
        }

        //get all student_id where id student does not exists on classroom table
        $fetch_student = Student::whereNotIn('id' , $student_id)->get();
        $fetch_teacher = Teacher::all();
        return view('classroom', compact('fetch_all_classroom', 'fetch_student', 'fetch_teacher'));
    }

    public function addClassroom(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
        ]);

        $add_Classroom = Classroom::insert([
            'name' => $request->name,
            'teacher_id' => $request->teacher_id,
            'student_id' => $request->student_id
        ]);
        return back();
    }

    public function editClassroom(Request $request)
    {
        // return $request->teacher_id;
        $validatedData = $request->validate([
            'name' => 'required',
        ]);

        $edit_Classroom = Classroom::where('id', $request->id)->update([
            'name' => $request->name,
            'teacher_id' => $request->teacher,
            'student_id' => $request->student
        ]);

        return back();
    }

    public function deleteClassroom($id)
    {
        $delete_Classroom = Classroom::find($id)->delete();

        return back();
    }

    public function exportToPDF()
    {
        $fetch_all_classroom = Classroom::all();
        $pdf = PDF::loadView('export' , compact('fetch_all_classroom'));

        return $pdf->download('classroom.pdf');

    }

}
