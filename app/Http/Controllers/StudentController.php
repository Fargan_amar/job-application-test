<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentController extends Controller
{
    public function index()
    {
        $fetch_all_student = Student::all();

        return view('student', compact('fetch_all_student'));
    }

    public function addStudent(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
        ]);

        $add_Student = Student::insert([
            'name' => $request->name
        ]);
        return back();
    }

    public function editStudent(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
        ]);

        $edit_Student = Student::find($request->id)->update([
            'name' => $request->name
        ]);

        return back();
    }

    public function deleteStudent($id)
    {
        $delete_Student = Student::find($id)->delete();

        return back();
    }

}
