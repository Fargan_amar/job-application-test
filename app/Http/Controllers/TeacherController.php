<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teacher;

class TeacherController extends Controller
{
    public function index()
    {
        $fetch_all_teacher = Teacher::all();

        return view('teacher', compact('fetch_all_teacher'));

    }

    public function addTeacher(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
        ]);

        $add_teacher = Teacher::insert([
            'name' => $request->name
        ]);
        return back();

    }

    public function editTeacher(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
        ]);

        $edit_teacher = Teacher::find($request->id)->update([
            'name' => $request->name
        ]);

        return back();

    }

    public function deleteTeacher($id)
    {
        $delete_teacher = Teacher::find($id)->delete();

        return back();
    }

}
