<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'name'
    ];

    public function classroom()
    {
        return $this->hasOne('App\Classroom');
    }
}
